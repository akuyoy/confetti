﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIupdate : MonoBehaviour {
	public Text cup1text;
	public Text cup2text;
	public Text caketext;

	public static UIupdate uiUpdateScript;

	void Start() {
		uiUpdateScript = this;
	}

	public void UpdateUI() {
		cup1text.text = "x" + GameController.cup1;
		cup2text.text = "x" + GameController.cup2;
		caketext.text = "x" + GameController.cakeBday;
	}
}
