﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col) {
		if (col.tag == "cup1") {
			GameController.cup1++;
			UIupdate.uiUpdateScript.UpdateUI ();
		}
		if (col.tag == "cup2") {
			GameController.cup2++;
			UIupdate.uiUpdateScript.UpdateUI ();
		}
		if (col.tag == "cake") {
			GameController.cakeBday++;
			UIupdate.uiUpdateScript.UpdateUI ();
		}
		if (col.tag == "letter") {
			GameController.count++;
		}
		Destroy (col.gameObject);
	}
}
