﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cakeDrop : MonoBehaviour {
	public int speed = 2;
	void Update () {
		transform.localPosition += Vector3.down * Time.deltaTime * speed;
		transform.Rotate (0, 0, 120 * Time.deltaTime);
	}
}
