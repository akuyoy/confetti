﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject[] cake;
	public GameObject[] letter;
	public GameObject complete;
	public GameObject control;

	public static int count = 0;
	public static int cup1 = 0;
	public static int cup2 = 0;
	public static int cakeBday = 0;

	float normalCounter = 2f;
	float specialCounter = 10f;

	bool gameOver = false;

	void Start() {
		//GameObject temp = Instantiate (cake [2]);
		//temp.transform.localPosition += new Vector3 (Random.Range (-2.5f, 2.5f), 0, 0);
		count = 0;
		cup1 = 0;
		cup2 = 0;
		cakeBday = 0;
	}

	void Update () {
		if (!gameOver) {
			if (normalCounter > 0) {
				normalCounter -= Time.deltaTime;
			} else {
				normalCounter = 2f;
				GameObject temp = Instantiate (cake [Random.Range (0,3)]);
				temp.transform.localPosition += new Vector3 (Random.Range (-2.5f, 2.5f), 0, 0);
			}

			if (specialCounter > 0) {
				specialCounter -= Time.deltaTime;
			} else {
				specialCounter = 7f;
				GameObject temp = Instantiate (letter [count]);
				temp.transform.localPosition += new Vector3 (Random.Range (-2.5f, 2.5f), 0, 0);
			}
			if (count == 13) {
				gameOver = true;
				complete.SetActive (true);
				control.SetActive (false);
			}
		}

	}

	public void goToMenu() {
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu");
	}
}
