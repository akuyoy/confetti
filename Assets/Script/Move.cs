﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	public Transform player;
	int direction = 0; // 0 - stop, 1 - left, 2 - right
	int speed = 5;
	float maxDist = 2.6f;

	public void Left() {
		direction = 1;
	}
	public void right() {
		direction = 2;
	}
	public void stop() {
		direction = 0;
	}

	void FixedUpdate() {
		
		if (player.localPosition.x > -maxDist && direction == 1) {
			player.localPosition -= Vector3.right * speed * Time.deltaTime;
		} else if (player.localPosition.x < maxDist && direction == 2) {
			player.localPosition += Vector3.right * speed * Time.deltaTime;
		}
		
	}
}
