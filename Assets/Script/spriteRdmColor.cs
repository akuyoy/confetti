﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spriteRdmColor : MonoBehaviour {

	SpriteRenderer sprite;

	void Start () {
		sprite = gameObject.GetComponent<SpriteRenderer> ();
		StartCoroutine (randomGenerate ());
	}

	IEnumerator randomGenerate() {
		while (true) {
			sprite.color = Random.ColorHSV();
			yield return new WaitForSeconds (0.1f);
		}
	}
}
