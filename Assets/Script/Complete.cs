﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Complete : MonoBehaviour {

	Text complete;

	void Start () {
		complete = gameObject.GetComponent<Text> ();
		StartCoroutine (randomGenerate ());
	}

	IEnumerator randomGenerate() {
		while (true) {
			complete.color = Random.ColorHSV();
			yield return new WaitForSeconds (0.1f);
		}
	}
}
