﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dontdestroy : MonoBehaviour {
	public static dontdestroy instance;
	void Awake () {
		DontDestroyOnLoad (this);
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}

}
